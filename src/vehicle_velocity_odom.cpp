// Include the ROS C++ APIs
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int16.h>

ros::Publisher odom_pub;

float received_vehicle_vel = 0.0; // m/s
ros::Time received_timestamp;

bool flag_velocity_received = false;
bool flag_timestamp_received = false;

void VehicleVelCallback(const std_msgs::Int16::ConstPtr& msg) {
    received_vehicle_vel = float(msg->data)/100.0;   // cm/s to m/s
    flag_velocity_received = true;
}

void NovatelCallback(const nav_msgs::Odometry::ConstPtr& msg){
    received_timestamp = msg->header.stamp;
    flag_timestamp_received = true;
}

// Standard C++ entry point
int main(int argc, char** argv) {

    // Initialize the ROS system
    ros::init(argc, argv, "vehicle_velocity");

    // Establish this program as a ROS node
    ros::NodeHandle nh;

    ros::Subscriber novatel_sub = nh.subscribe("/odom", 1, NovatelCallback);
    ros::Subscriber vehicle_vel_sub = nh.subscribe("/vehicle_state/velocity", 1, VehicleVelCallback);
    ros::Publisher odom_pub = nh.advertise<nav_msgs::Odometry>("/vehicle_state/velocity/odom", 1);

    nav_msgs::Odometry velocity_odom;
    velocity_odom.header.frame_id = "odom";
    velocity_odom.child_frame_id = "odometer";

    ros::Rate rate(10); // 10hz


    while(ros::ok())
    {
        if(flag_timestamp_received && flag_velocity_received) {
            velocity_odom.header.stamp = received_timestamp;
            velocity_odom.twist.twist.linear.x = 0.0;
            velocity_odom.twist.twist.linear.y = received_vehicle_vel;
            velocity_odom.twist.twist.linear.z = 0.0;
            odom_pub.publish(velocity_odom);
        }

        ros::spinOnce();
        rate.sleep();
    }
}