#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>

bool first_call = true;
ros::Time odom_stamp;
ros::Time filterOdom_stamp;
tf::Transform tf_worldToInit;
tf::Transform tf_worldToVehicle;
tf::Transform tf_worldToFilter;

void OdomCallBack(const nav_msgs::Odometry::ConstPtr& msg) {
 	
 	if (first_call) {
 		odom_stamp = msg->header.stamp;

 		tf_worldToInit.setOrigin( tf::Vector3(msg->pose.pose.position.x,
 		                                      msg->pose.pose.position.y,
 		                                      msg->pose.pose.position.z) );
	
		tf_worldToInit.setRotation( tf::Quaternion(msg->pose.pose.orientation.x,
											   	   msg->pose.pose.orientation.y,
											       msg->pose.pose.orientation.z,
											       msg->pose.pose.orientation.w) );
		first_call = false;
	}

 	odom_stamp = msg->header.stamp;

 	tf_worldToVehicle.setOrigin( tf::Vector3(msg->pose.pose.position.x,
 		                                     msg->pose.pose.position.y,
 		                                     msg->pose.pose.position.z) );
	
	tf_worldToVehicle.setRotation( tf::Quaternion(msg->pose.pose.orientation.x,
											   	  msg->pose.pose.orientation.y,
											      msg->pose.pose.orientation.z,
											      msg->pose.pose.orientation.w) );

 }

 void FilterOdomCallBack(const nav_msgs::Odometry::ConstPtr& msg) {
 	
 	filterOdom_stamp = msg->header.stamp;

 	tf_worldToFilter.setOrigin( tf::Vector3(msg->pose.pose.position.x,
 		                                    msg->pose.pose.position.y,
 		                                    msg->pose.pose.position.z) );
	
	tf_worldToFilter.setRotation( tf::Quaternion(msg->pose.pose.orientation.x,
											   	 msg->pose.pose.orientation.y,
											     msg->pose.pose.orientation.z,
											     msg->pose.pose.orientation.w) );

 }

int main(int argc, char** argv) {
	
	ros::init(argc, argv, "robot_tf_publisher");
	ros::NodeHandle nh;

	ros::Rate r(100); // publish that in 100Hz

	// create a subscriber for odom topic to update transformation between /world and /gps_frame
	ros::Subscriber odom_sub = nh.subscribe("/odom", 1, OdomCallBack);
	ros::Subscriber filterOdom_sub = nh.subscribe("/global_odometry/filtered", 1, FilterOdomCallBack);

	tf::TransformBroadcaster broadcaster;

	while(nh.ok()) {

		if (!first_call) {
			broadcaster.sendTransform(tf::StampedTransform(tf_worldToInit, odom_stamp, "world", "init_rviz"));

			broadcaster.sendTransform(tf::StampedTransform(tf_worldToVehicle, odom_stamp, "world", "gps_frame_rviz"));

			broadcaster.sendTransform(tf::StampedTransform(tf_worldToFilter, filterOdom_stamp, "world", "ekf_rviz"));

		}

		ros::spinOnce();
		r.sleep();

	}
}
