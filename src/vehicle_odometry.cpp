// Include the ROS C++ APIs
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Int16.h>

ros::Publisher odom_pub;

double received_angular_vel[3]; // rad/s
double received_vehicle_vel = 0.0; // m/s
ros::Time received_timestamp;

bool flag_imu_received = false;
bool flag_velocity_received = false;
bool flag_timestamp_received = false;

void ImuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
    if(!flag_imu_received) {
        flag_imu_received = true;
    }

    // received_timestamp = msg->header.stamp;

    received_angular_vel[0] = msg->angular_velocity.x;
    received_angular_vel[1] = msg->angular_velocity.y;
    received_angular_vel[2] = msg->angular_velocity.z;
}

void VehicleVelCallback(const std_msgs::Int16::ConstPtr& msg) {

    if(!flag_velocity_received) {
        flag_velocity_received = true;
    }

    // if(msg->data > 70){
    //     received_vehicle_vel = float(msg->data)/100.0;   // cm/s to m/s
    // }
    // else{
    //     received_vehicle_vel = 0.0; // value lower than 70 has errors
    // }

    received_vehicle_vel = double(msg->data)/100.0;// cm/s to m/s
}

void NovatelGPSCallback(const nav_msgs::Odometry::ConstPtr& odom_msg){
    if(!flag_timestamp_received) {
        flag_timestamp_received = true;
    }
    received_timestamp = odom_msg->header.stamp;
}


// Standard C++ entry point
int main(int argc, char** argv) {

    // Initialize the ROS system
    ros::init(argc, argv, "vehicle_odometry");

    // Establish this program as a ROS node
    ros::NodeHandle nh;

    ros::Subscriber imu_sub = nh.subscribe("/imu/data", 1, ImuCallback);
    ros::Subscriber vehicle_vel_sub = nh.subscribe("/vehicle_state/velocity", 1, VehicleVelCallback);
    ros::Subscriber odom_sub = nh.subscribe("/odom", 1, NovatelGPSCallback);
    ros::Publisher odom_pub = nh.advertise<nav_msgs::Odometry>("/vehicle_odometry", 1);

    nav_msgs::Odometry velocity_odom;
    velocity_odom.header.frame_id = "/world";
    velocity_odom.child_frame_id = "/vehicle_odometry";

    ros::Rate rate(100); // 100hz

    for(int i=0; i<3; i++) {
        received_angular_vel[i] = 0;
    }

    while(ros::ok())
    {
        if(flag_imu_received && flag_velocity_received && flag_timestamp_received) {
            // xsens imu frame: x:forward, y:left, z:up
            // vehicle frame: x:right, y:forward, z:up
            velocity_odom.header.stamp = received_timestamp;
            velocity_odom.twist.twist.linear.x = 0.0;
            velocity_odom.twist.twist.linear.y = received_vehicle_vel;
            velocity_odom.twist.twist.linear.z = 0.0;
            velocity_odom.twist.twist.angular.x = -received_angular_vel[1];
            velocity_odom.twist.twist.angular.y =  received_angular_vel[0];
            velocity_odom.twist.twist.angular.z =  received_angular_vel[2];
            odom_pub.publish(velocity_odom);
        }

        ros::spinOnce();
        rate.sleep();
    }
}