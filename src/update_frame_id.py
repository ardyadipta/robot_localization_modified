import rosbag
import sys

if len(sys.argv) < 2:
    print sys.argv[0], ' <input-bag> <output-bag>'
    print 'Need atleast 2 arguments'
    sys.exit(-1);
else:
    print 'Input:',sys.argv[1]
    print 'Output:',sys.argv[2]

with rosbag.Bag(sys.argv[2],'w') as outbag:
    for topic, msg, t in rosbag.Bag(sys.argv[1]).read_messages():
        if topic == "/garmin_gps/odom":
            msg.header.frame_id = 'world';
            msg.child_frame_id = 'garmin_gps';

        outbag.write(topic,msg,t)

